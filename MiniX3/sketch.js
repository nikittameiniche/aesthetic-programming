function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(12); //larger number makes the illusion of spinning faster
}

function draw() {
  background(107,126,103,50); //last number = transparency (larger number makes for fast moving)
  drawFlower(); //tells the fuction draw, to run this particular function
}

function drawFlower() { //defines a new function

//flower petals small
push(); //point of origin will only affect this code
let num = 15; //how close the ellipses are to eachother
translate(width/2,height/2); //new point of origin
let cir = 360/num*(frameCount%num);
rotate(radians(cir)); //rotating everithing that follows
noStroke();
fill('white');
ellipse(60,0,50,15); //small petal 1
ellipse(-60,0,50,15); //small petal 2
ellipse(0,-70,20,70); //large petal 1
ellipse(0,70,20,70); //large petal 2
pop(); //point of origin stopped

//flower grows big
if(mouseIsPressed){
push();
let num = 15; //how close the ellipses are to eachother
translate(width/2,height/2);
let cir = 360/num*(frameCount%num);
rotate(radians(cir)); //rotating the petals
noStroke();
fill('white');
ellipse(80,0,90,25); //small petal 1
ellipse(-80,0,90,25); //small petal 2
ellipse(0,-90,30,110); //large petal 1
ellipse(0,90,30,110); //large petal 2
pop();
}

//text under flower
else {
textSize(15);
textFont('courier');
fill('white');
text("P R E S S  T O  W A T C H  M E  G R O W",width/3+30,height/4*3);
}

//flower bud
push();
let amount = 15; //how close the circles are to eachother
translate(width/2,height/2);
let bud = -360/amount*(frameCount%amount);
rotate(radians(bud));
noStroke();
fill(252,220,0);
ellipse(0,0,55,55); //flower bud
stroke(0);
strokeWeight(3);
arc(0,2,30,28,0,PI); //mouth
noStroke();
fill(0);
circle(7,-10,6); //eye 1
circle(-7,-10,6); //eye 2
fill('white');
pop();
}
