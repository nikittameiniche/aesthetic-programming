# MiniX3

<img src="flower1.png" width="630" height="400">

**Video of the throbber:**
![](flower.mov)

## URL to work in web browser
https://nikittameiniche.gitlab.io/aesthetic-programming/MiniX3/

## Link to the sketch file
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX3/sketch.js

## Explaining my program
For this week I have created my MiniX3, which is a throbber that resembles a daisy flower spinning around. I thought about what a throbber meant to me, and I came to the conclusion that I associate a throbber with something that is alive in some way and can move. Because of this my inspiration for this project has come to be a living flower. At first the flower has a certain size, but if the mouse is pressed, the flower will grow bigger. The reason why I included this feature, was because I felt that it made the throbber just a tad bit more interesting, without moving away from the simple look of the throbber on the screen. When I was writing my code, I started out using a background that is somewhat transparent by adding a level of transparency after my desired color in the background parentheses, as this is what can create the illusion of a moving shape. Then I decided to try and make my own function “drawFlower”, just to experience how that worked. In this function I wrote in the petals using two transformational functions; the translate function and the rotate function, to rotate four different sized white ellipses around the center of the canvas as the new point of origin, to create the illusion of a flower that blooms or moves. To make the flower grow by pressing the mouse, I added an if-statement, where I used the same model with larger sized ellipses, stating that if the mouse is pressed, then the flower should show the bigger petals. I decided to include a text, that is always shown as an else statement (whenever the mouse is not being pressed), telling you to press the mouse to watch the flower grow.  

What was interesting about this sketch, was that in the beginning I added things like raindrops that moved with the mouse and a sun with moving rays of light, but then I ended up thinking that it looked way to messy and that there was too much going on, on the canvas. As a result of this, I took a step back and decided that it was actually more aesthetically pleasing, when it was simpler, and the throbber was the main point of focus. 

## Throbbers and the perception of time
The throbber is something interesting as it represents time passing and while it runs, you have no idea when you should expect it to end. It is worth looking at how we create a sense of time, in my MiniX I have used the layout of a common throbber, with a few alterations to make it look like a flower. In a way I think the throbber is more effective than fx using a timer, that would keep on adding more minutes or seconds to the “loading period”, as this is something that can really frustrate you when you are updating things like your computer. In that sense, the throbber is quite nice, because we don’t realize how much time has passed since it showed up. One reading from this week was the chapter "The ends of time" from the book "The Techno-Galactic Guide to Software Observation", 2018. The chapter mentions a change in how we perceive time since the early days, when sunrise and sunset was what defined the length of our day. Nowadays with electricity being available, our day has been elongated, and we no longer rely on the sun. This notion is quite fascinating, and I think the question of whether the computer could run on sundial time is a good one. The pros would be that we could reintroduce the natural cycle of the day, and this could force us to work when it was daytime only, as the computer would shut down when the sun has set. The new way of working has given people the flexibility to arrange their daily events how they want over a longer period of time, and to many people it might feel like losing half of the day, if the sundial time was applied to their computer or other electronics. When we think about it, we have made it so, that we have more hours in a day than our ancestors, which is both incredible and probably not that good for us, as we need to sleep, and end up pushing our sleep or not sleeping much.  

## A throbber from my everyday life
When we think of a throbber the first one that comes to mind is probably the one with dots going around in a circle, each disappearing and reappearing one after another. But there are many throbbers and the one that was sort of different that I came to think of, was this one from Netflix:

<img src="netflix.png" width="200" height="400">

This throbber shows some blinking shapes, such as circles and rectangles, which are supposed to resemble the content that is about to appear. What I feel this throbber communicates is the loading and/or the custumization of movies and series that fit with the things you tend to stream. Every time you open up the app, Netflix knows what content it has to offer, but I think what is happening is that it takes a few seconds for the algorithm to decide what content to display and in which order. Im not sure that this is really what is going on behind the throbber, in reality it might be something similar to the facebook one, where it is mainly there for the purpose of making people believe that, the loading of content is what it symbolizes.   


