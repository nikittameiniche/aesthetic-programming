//Sources:
//Font - https://www.dafont.com/retro-gaming.font
//Typewriter effect - https://gist.github.com/mjvo/2dce29799eb75b7ee1a571380f12ef1b?fbclid=IwAR1hjLiI_IICeA2HAvyG6cADSnyGXNY9l8UVDAxBdck9Yg2TZvZfL0Z7d8U

//declaring global variables
let robot;
let crystal;
let glow = 10;
let pace = 0.5;
let question = "WOULD YOU LIKE TO KNOW HOW THE FUTURE WILL BE?";
let textShown = "";
let charRNN;
let fortune;
let button;
let guide;
let txt;


function preload() {
  robot = loadImage("robot.png");
  crystal = loadImage("crystal.png");
  font = loadFont('retrogaming.ttf');
}

function setup() {
  createCanvas(1300, 700);

  //loading the trained model
  charRNN = ml5.charRNN('./fortunes');

  textAlign(CENTER);
  textSize(20);
  textFont(font);
  fill(255);
  typeWriter(question, 0, 650, 100, 150);  

  //guiding text
  guide = createDiv('PRESS AGAIN FOR ANOTHER FORTUNE');
  guide.position(545, 625);
  guide.style('color', '#a7a7a7');
  guide.style('font-size', '10px');
  guide.style('font-family', 'retrogaming');
  guide.style('text-transform', 'uppercase');
  guide.hide();

  //button to generate a fortune
  button = createButton('TELL ME ABOUT THE FUTURE');
  button.position(510, 580);
  button.size(300, 35);
  button.style('background-color', '#101010');
  button.style('color', '#ffffff');
  button.style('font-size', '16px');
  button.style('font-family', 'retrogaming');
  button.style('outline-color', '#bb34fa');
  button.hide();
  //when button is pressed, run the function generate
  button.mousePressed(generate);

  //creating a text div to store the fortune generated
  fortune = createDiv("");

  function generate() {
    fill(0);
    rect(width / 2, 100, 800, 100);
    guide.show();
    charRNN.generate({
      seed: 'future',
      length: 100,
      temperature: 0.15
    }, function(err, results) {
      txt = results.sample;         
      fortune.html(txt);
      fortune.position(330, 50);
      fortune.style('width', '50%');
      fortune.style('color', '#ffffff');
      fortune.style('font-size', '20px');
      fortune.style('font-family', 'retrogaming');
      fortune.style('text-transform', 'uppercase');
      fortune.style('text-align','CENTER');
    });
  }


}

function draw() {
  background(0);

  //displaying the robot and crystal ball
  imageMode(CENTER);
  image(robot, width / 2, height / 2);
  image(crystal, width / 2 + 8, 460);

  //glowing crystal ball
  push();
  translate(660, 443);
  noFill();
  strokeWeight(glow);
  stroke(187, 52, 250, 100);
  ellipse(0, 0, 150, 150);
  pop();

  if (glow > 16 || glow < 10) {
    pace = pace * -1;
  }
  glow = glow + pace;

  //writes question from robot
  text(textShown, 650, 100);
}

function typeWriter(sentence, n, x, y, speed) {     
  background(0);
  if (n < (sentence.length)) {
    textShown = sentence.substring(0, n + 1);
    n++;
    setTimeout(function() {
      typeWriter(sentence, n, x, y, speed)
    }, speed);
  } else {
    button.show();
  }
}
