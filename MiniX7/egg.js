class Egg {
  constructor() {
    this.d = floor(random(40, 60)); //diameter of egg horisontal
    this.x = width;
    this.y = height - 40;
    this.fill = color(random(155,255),random(155,255),random(155,255));
  }

  move() {
    this.x -= 16; //makes the egg move bacwards
  }

  show() {
    fill(this.fill); //each new egg has new random color
    noStroke();
    ellipse(this.x, this.y, this.d, this.d+this.d/3); //displays egg
  }
}
