# MiniX7

<img src="photo1.png" width="550" height="400">

<img src="photo2.png" width="550" height="400">

<img src="video.mov" width="550" height="400">

## URL to run program in web browser
https://nikittameiniche.gitlab.io/aesthetic-programming/MiniX7/

## Link to the files containing the code
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX7/sketch.js

https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX7/rabbit.js

https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX7/egg.js

## How my game works

The game that I made this week, is a simple game, where you control a character to jump over the incoming obstacles. The character in my game is an easter bunny, that can jump if you press "space". The obstacles are easter eggs, that move accros the screen towards the bunny. If what the purpose of the game is, is to jump over as many obstacles as possible and therefore not hit any eggs. If you touch an egg, the game is over.

## How I programmed the game

First of all I was inspired to make this game by Daniel Shiffman, who in [this video](https://youtu.be/l0HoJHc-63Q) explains the basic idea of programming the game. 

I programmed the easter bunny and the easter eggs both as class objects with each having their own js file. The bunny was created as a class rabbit, using a constructor, in which I added a number of variables, that only apply to this bunny. The bunny is an object, that has a size property. This class rabbit also stores data about when the bunny can jump and how to detect whether the bunny has hit an egg. For the eggs, I also made a class egg, using a constructor, to give the eggs variables for its properties size, color and speed. For both of these objects I used the methods/behaviors move() and show(), which means that when these functions are called for the bunny, it will show the bunny on the bottom of the screen, and make it move/jump upwards, when the space key is pressed. For the egg, it will show the eggs on the right side of the program and then they will start to move accros the canvas as long as the draw function is running. Like Shiffman explains in his video, I used the collision library for p5, in order to make the program detect when the bunny collides with the eggs, by drawing a circle around each of them, that if touching will result in game over.    

## About object-oriented programming (OOP)

In The Obscure Objects of Object Orientation by Fuller and Goffey, it is described how many of the most used programming languages use OOP and it is something that the programmers always have in the back of their minds. Object-oriented programming is a programming system where we use "objects", that can contain data and code. Data might be any attributes or properties, and code is the behaviours of the objects or the methods. According to the Aesthetic Programming Handbook, one of the main characteristics of OOP is abstraction, a way of programming, where the goal is to handle complexity of objects by showing only the necissary attributes, while leaving out unnecissary ones. In other words, the detailed objects and how we know them, are being abstracted, to the extent that they are missing some datails, but are still recognizable. 

## My game in a cultural context and the abstraction of objects

If I were to discuss my game in a wider cultural context, I think it is worth mentioning that I dont think these kinds of games are very relevant anymore. The game that I made for this MiniX and similar games, are games that you could find many years back, when I was young and even when my parents were growing up. They are based on simple movements and looks, which was obvioulsy working back then, becuase my friends and I, could play games like these for hours. However, now people expect something more from games. The expect many details and maybe even 3D worlds, in which they can walk around and explore, doing different tasks and activities. Gaming culture has changed so much that these mini games are no longer relevant or interesting enough. Also what we want to achieve by playing the games is no longer the same. Back in the day the goal was to beat your own or your friend´s high score, whereas now with more realistic games, people can almost disappear into another world, living a second life in the game. Today many games are about socializing and taking a break from reality, some people even seem to create a new "better" life, in the games.

Unlike all of the new games that have emerged, my game uses a high amount of abstraction. In my game, The eggs are being abstracted, as they might have more detail in real life and aren’t normally able to move, but they are still recognizable as easter eggs. We have this ability to regognize objects with little details or information, knowing an egg and applying a color to it, I think would by most people be recognised as an easter egg. Annother thing that I have abstracted is the movement of the easter bunny, which has been limited. A real bunny would be able to jump with different heights, directions and acceleration. This abstraction is present in many games, a goog example would be human characters. Creating a character resembling a human is forces yout to choose which properties to include. For this reason the most important details are selected and icluded in the object data. The programmer might be asking him-/herself whether eye color, neck length or eyebrows are all important for the recognizability. The intereseting aspect of abstraction, I think, is the fact that we are able to recognize a character as fx. humanlike, with only a small number of details. 
