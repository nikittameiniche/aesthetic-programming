class Rabbit {
  constructor() {
    this.d = 120 ; //diameter of circle
    this.x = 50;
    this.y = height - this.d;
    this.vy = 0; //velocity of speed along the y-axis
    this.gravity = 3; //adjusting the speed of rabbit jump
  }

  jump() {
    //this only allows jump function to be called, when unicorn is touching the "ground"
    if (this.y == height - this.d) {
    //if jump function is activated, then unicorn moves up y-axis
    this.vy = -35;
    }
  }

  hits(egg) {
    //making collision circles for the unicorn and the train, with diameter d.
    let x1 = this.x + this.d * 0.5;
    let y1 = this.y + this.d * 0.5;
    let x2 = egg.x + egg.d * 0.5;
    let y2 = egg.y + egg.d * 0.5;
    return collideCircleCircle(x1, y1, this.d, x2, y2, egg.d);
  }

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
    //can only move up to a certain point
    this.y = constrain(this.y, 0, height - this.d);
  }

  show() {
    //displays the rabbit avatar
    image(rabbitImg, this.x, this.y, this.d, this.d);
  }
}
