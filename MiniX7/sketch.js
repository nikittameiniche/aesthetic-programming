let rabbit;
let rabbitImg; //rabbit character drawing
let scenery; //bacground scenery drawing
let eggs = []; //egg array
let score = 0;

function preload() {
  rabbitImg = loadImage('rabbit.png');
  scenery = loadImage('background.jpg');
}

function setup() {
  createCanvas(800, 600);
  rabbit = new Rabbit();
}

function keyPressed() {
  if (key == ' ') { //space key down
    rabbit.jump(); //calling the jump function from rabbit.js
  }
}

function displayScore(){
  fill(255);
  textSize(20);
  textStyle(NORMAL);
  textAlign(CENTER);
  text("SCORE = " + score, width-140, 50); //showing score in right corner
}

function draw() {
  if (random(1) < 0.008) { //0,8% of the time push new egg
    eggs.push(new Egg());
  }

  background(scenery);
  fill(255);
  textSize(12);
  text("PRESS SPACE TO JUMP", width-140,80);
  noStroke();
  fill(255,80);
  rectMode(CENTER);
  rect(width-140,75,200,20);
  for (let e of eggs) {
    e.move();
    e.show();
    if (rabbit.hits(e)) { //if rabbit hits an egg the game ends
      noLoop();
      textSize(50);
      textAlign(CENTER);
      textStyle(BOLD);
      fill(255);
      text("GAME OVER",width/2,height/2);
      textSize(20);
      text("BETTER LUCK NEXT TIME",width/2,height/2+40);
   }
    if (rabbit.hits(e)==false){
      score++;
    }
  }

  rabbit.show();
  rabbit.move();
  displayScore();
}
