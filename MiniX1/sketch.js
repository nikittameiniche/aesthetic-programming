function setup() {
  // put setup code here
createCanvas(476,612);
print("hello world");
}

function draw() {
  // code moving from the top to the bottom in rows, from left to right.

//general settings:
background(232,226,220); //background color
strokeWeight(4); //thickness of lines
stroke('white'); //changes color of all lines
noFill();
quad(34,34,442,34,442,578,34,578); //square (meaning: 4 pairs of coordinates)

//first row:
circle(102,102,68); //yellow circle (meaning: x,y,d)
fill(254,239,106); //coloring a shape
circle(102,102,68);
line(102,136,102,170); //line vertical
arc(102, 170, 68, 68, 7, PI + QUARTER_PI); //orange halfcircle
fill(255,171,47);
arc(102, 170, 68, 68, 7, PI + QUARTER_PI);
circle(102,238,68); //green circle
fill(132,253,121);
circle(102,238,68);
circle(102,238,34); //inner circle
fill(255,136,188);
circle(102,238,34);
line(102,272,102,304); //line vertical
arc(102,306,68,68, 75, PI + QUARTER_PI); //lightblue circle cutout
fill(140,210,214);
arc(102,306,68,68, 75, PI + QUARTER_PI);
circle(102,374,68); //red circle
fill(254,87,90);
circle(102,374,68);
circle(102,374,34); //inner circle
fill(140,210,214);
circle(102,374,34);
line(34,374,68,374); //horisontal line
line(102, 408, 102, 476); //line vertical (meaning: x1,y1,x2,y2)
line(102, 408, 102, 476);
circle(102,510,68); //purple cicle
fill(204,136,254);
circle(102,510,68);
circle(102,510,34); //inner circle
fill(254,239,106);
circle(102,510,34);

//second row:
line(170,34,170,136); //line vertical
noFill(); //removes color from figure
triangle(136,68,204,68,170,136); //triangle (meaning:x1,y1,x2,y2,x3,y3)
circle(170,170,68); //pink circle
fill(255,136,188);
circle(170,170,68);
arc(170, 238, 68, 68, 11, PI + QUARTER_PI); //blue circle cutout
fill(68,165,253);
arc(170, 238, 68, 68, 11, PI + QUARTER_PI);
line(170,272,170,374); //line vertical
arc(170, 374, 68, 68, 7, PI + QUARTER_PI); //orange halfcircle
fill(255,171,47);
arc(170, 374, 68, 68, 7, PI + QUARTER_PI);
circle(170,442,68); //green circle
fill(132,253,121);
circle(170,442,68);
line(170,476,170,510); //line vertical
arc(170,510,68,68, 75, PI + QUARTER_PI); //yellow circle cutout
fill(254,239,106);
arc(170,510,68,68, 75, PI + QUARTER_PI);
line(170,544,170,578);

//third row:
line(238,34,238,102); //line vertical
arc(238,102,68,68, 75, PI + QUARTER_PI); //green circle cutout
fill(132,253,121);
arc(238,102,68,68, 75, PI + QUARTER_PI);
circle(238,170,68); //lightblue circle
fill(140,210,214);
circle(238,170,68);
noFill();
quad(238,204,272,238,238,272,204,238);
line(204,238,272,238);
line(238,204,238,306); //line vertical
arc(238,306,68,68, 7, PI + QUARTER_PI); //purple halfcircle
fill(204,136,254);
arc(238,306,68,68, 7, PI + QUARTER_PI);
circle(238,374,68); //yellow circle
fill(254,239,106);
circle(238,374,68);
circle(238,374,34); //inner circle
fill(204,136,254);
circle(238,374,34);
line(238,408,238,442); //line vertical
arc(238,442,68,68, 75, PI + QUARTER_PI); //blue circle cutout
fill(68,165,253);
arc(238,442,68,68, 75, PI + QUARTER_PI);
circle(238,510,68); //pink circle
fill(255,136,188);
circle(238,510,68);

//fourth row:
circle(306,102,68); //blue circle
fill(68,165,253);
circle(306,102,68);
circle(306,102,34); //inner circle
fill(255,171,47);
circle(306,102,34);
line(306,136,306,204); //line vertical
arc(306, 238, 68, 68, 11, PI + QUARTER_PI); //red circle cutout
fill(254,87,90);
arc(306, 238, 68, 68, 11, PI + QUARTER_PI);
circle(306,306,68); //orange circle
fill(255,171,47);
circle(306,306,68);
line(306,340,306,374); //line vertical
arc(306,374,68,68, 75, PI + QUARTER_PI); //lightblue circle cutout
fill(140,210,214);
arc(306,374,68,68, 75, PI + QUARTER_PI);
line(306,408,306,578); //line vertical
noFill();
triangle(272,544,306,476,340,544);

//fifth row:
circle(374,102,68); //purple circle
fill(204,136,254);
circle(374,102,68);
line(374,136,374,170); //vertical line
arc(374,170,68,68, 7, PI + QUARTER_PI); //orange halfcircle
fill(255,171,47);
arc(374,170,68,68, 7, PI + QUARTER_PI);
circle(374,238,68); //pink circle
fill(255,136,188);
circle(374,238,68);
circle(374,238,34); //inner circle
fill(132,253,121);
circle(374,238,34);
line(408,238,442,238); //horisontal line
noFill();
quad(374,340,408,374,374,408,340,374);
line(340,374,442,374); //horisontal line
line(374,272,374,408); //vertical line
arc(374,442,68,68, 11, PI + QUARTER_PI); //red circle
fill(254,87,90);
arc(374,442,68,68, 11, PI + QUARTER_PI);
circle(374,510,68); //lightblue circle
fill(140,210,214);
circle(374,510,68);
circle(374,510,34); //inner circle
fill(254,87,90);
circle(374,510,34);

}
