# MiniX1

![](MiniX1.png)

## URL to work in web-browser
http://nikittameiniche.gitlab.io/aesthetic-programming/MiniX1/

## Link to the sketch file
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX1/sketch.js

## About my program
This program is an abstract piece of art, which explores geometric shapes and contrast colors. 

The program utilizes programming syntax, to make sure that the computer understands the code. I have worked with specific parts of the syntax, one being shapes such as circles, quads, triangles and lines. Other parts being the determination of color, size, placement and width. 

Creating this sketch has taught me that using mathematic skills is necessary in coding, as it enables you to calculate the correct values of each parameter, which will ensure that your shapes show up in the desired spot on the canvas. Besides this, I have learnt the general syntax for most of the shapes, including what each of their coordinate sets are composed of. Furthermore, I finally understood the use of RGB color scales, as this scale allowed me to select very specific colors for my sketch, instead of using the values ‘blue’, ‘red’ or ‘green’ which in p5.js leaves you with preselected standard colors. Lastly, this sketch taught me that programming can be very time consuming, especially right know, when I have to learn the syntax from scratch, without any previous knowledge.

## Reflections about coding
I started out using the references from the p5.js library on my own and modifying them, because I was convinced that it would probably make me remember more of it, than if I used someone else´s code. The difference is that you write it so many times when you write it yourself, that it is repeated both in your brain and through your fingers. However, reading someone else´s code can also help inspire your own code, or help resolve issues that you are dealing with when coding. For example, if you want to code something specific and you don’t know how, looking at other people´s code, might help you gain an understanding of the necessary code, and hopefully you can use that to code something similar yourself.   

Writing and coding are similar in the way that both have their own syntax and can be typed and interpreted. The major difference lies in the fact that writing is meant to be read directly by the human eye and understood as you read it, whereas code is written to ensure that the computer can make sense of it and turn it into a visual. 

Programming is a language like many others, and it has only become more important as time has passed. As explained in the first chapter of Anette Vee´s book, the term literacy is commonly defined as the ability to read and write. However, this chapter mentions that the definition of literacy might be undergoing some change in the 21st century. There seems to be a suggestion that coding is literacy, and that it is just as much of an essential skill as writing and reading. In other words, we also need to read and write code, because it can make us feel empowered, teach us to think more creatively and it will most likely be necessary to be employed in specific places. The importance and growth of technology is unquestionable, and because of this development there is no doubt in my mind that coding will soon enough be added to the definition of what it means to be literate.

