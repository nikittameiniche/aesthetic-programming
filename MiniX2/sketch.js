//defining variables (in this case the images)
let sanitizer
let earth
let puddle
let thumb

//loads the images so they are ready for use in the code
function preload() {
sanitizer = loadImage('sanitizer1.png');
earth = loadImage('earth.png');
puddle = loadImage('puddle.png');
thumb = loadImage('thumb.png');
}

function setup() {
  // put setup code here
createCanvas(640,420);
print("virus");

}

function draw() {
  // put drawing code here
background(240);
noStroke();
//the ground
quad(0,420,0,265,640,265,640,420);
fill(200);
quad(0,420,0,265,640,265,640,420);

//the earth emoji
//earth body (globe)
imageMode(CENTER);
image(earth,435,210,182,182);
//earth running legs
line(450,300,460,340);
strokeWeight(10);
stroke(58,115,236);
line(450,300,460,340);
ellipse(465,342,20,5);
fill(58,115,236);
ellipse(465,342,20,5);
line(415,295,400,320);
line(400,320,360,315);
ellipse(360,320,5,20);
//speedlines earth
strokeWeight(1);
stroke(0);
line(350,215,320,215);
line(360,230,315,230);
line(350,240,300,240);
line(345,320,330,320);
line(345,330,335,330);

//virus emoji body
noStroke();
circle(213,210,150);
fill(243,25,75);
circle(213,210,150);

//mouse on virus causes it to be disgusted and slip
if (mouseX > 138 && mouseY > 135 && mouseX < 288 && mouseY < 285) {
//virus sqinty eyes
textSize(30);
textStyle(BOLD);
fill(0);
text(">  <",190,230);
//virus mouth
noStroke();
arc(215,245,40,25,0,PI);
fill(0);
arc(215,245,40,25,0,PI);
triangle(204,245,209,245,207,248);
fill('white');
triangle(204,245,209,245,207,248);
triangle(221,245,226,245,224,248);
arc(215,250,20,30,0,PI);
fill(254,156,177);
arc(215,250,20,30,0,PI);
line(215,250,215,260);
strokeWeight(2);
stroke(0);
line(215,250,215,260);
//virus eyebrows
strokeWeight(3);
line(210,215,200,205);
line(220,215,230,205);
noStroke();
//puddle
imageMode(CENTER);
image(puddle,195,340);
//virus legs slipping
line(225,280,230,315);
strokeWeight(10);
stroke(243,25,75);
line(225,280,230,315);
line(230,315,195,330);
ellipse(190,335,5,20);
fill(243,25,75);
ellipse(190,335,5,20);
line(180,280,165,315);
line(165,315,130,310);
ellipse(130,315,5,20);
//earth mouth happy
noStroke();
arc(435,240,40,30,0,PI);
fill(0);
arc(435,240,40,30,0,PI);
arc(435,242,34,20,0,PI);
fill('white');
arc(435,242,34,20,0,PI);
line(420,246,450,246);
stroke(0);
strokeWeight(2);
line(420,246,450,246);
//earth eyes happy
strokeWeight(4);
arc(415,220,16,12, PI,TWO_PI);
fill(99,233,60);
arc(415,220,16,12, PI,TWO_PI);
arc(455,220,16,12, PI,TWO_PI);
fill(58,115,236);
arc(455,220,16,12, PI,TWO_PI);
//earth brows
noStroke();
fill(0);
arc(415,208,25,7, PI,TWO_PI);
arc(455,208,25,7, PI,TWO_PI);
//earth thumbs up
imageMode(CENTER);
image(thumb,505,260);
//cheeks earth
ellipse(400,230,17,9);
fill(253,62,113);
ellipse(400,230,17,9);
ellipse(470,230,17,9);
}

//virus chasing earth evil face
else {
//virus evil eyes + pupils
noStroke();
ellipse(200,222,15,20);
fill(0);
ellipse(200,222,15,20);
ellipse(230,222,15,20);
ellipse(202,220,7,7);
fill('white');
ellipse(202,220,7,7);
ellipse(232,220,7,7);
//virus eyebrows
strokeWeight(5);
stroke(0);
line(210,215,185,205);
line(220,215,245,205);
//virus mouth
noStroke();
arc(215,245,50,20,0,PI);
fill(0);
arc(215,245,50,20,0,PI);
triangle(204,245,209,245,207,250);
fill('white');
triangle(204,245,209,245,207,250);
triangle(221,245,226,245,224,250);
//virus legs running
line(225,278,240,340);
strokeWeight(10);
stroke(243,25,75);
line(225,278,240,340);
ellipse(245,342,20,5);
fill(243,25,75);
ellipse(245,342,20,5);
line(180,280,165,315);
line(165,315,130,310);
ellipse(130,315,5,20);
//earth mouth scared
noStroke();
fill(0);
ellipse(435,250,35,25);
ellipse(435,256,17,9);
fill(253,62,113);
ellipse(435,256,17,9);
fill('white');
arc(435,245,24,10, PI,TWO_PI);
//earth brows
fill(0);
arc(416,205,15,8, PI,TWO_PI);
arc(454,205,15,8, PI,TWO_PI);
//earth eyes
noStroke();
ellipse(420,222,15,20);
fill(0);
ellipse(420,222,15,20);
ellipse(450,222,15,20);
noStroke();
ellipse(418,222,5,9);
fill('white');
ellipse(418,222,5,9);
ellipse(448,222,5,9);
//speed lines virus
stroke(0);
strokeWeight(1);
line(110,220,70,220);
line(130,230,60,230);
line(110,245,80,245);
line(115,315,100,315);
line(115,325,110,325);
}

noStroke();

push();
//rotation of purple rectangles (virus arms)
translate(213,210);
for (let i = 0; i < 20; i ++) {
  rect(72,5,25,10);
  fill(243,25,75);
  rotate(PI/5);
}
pop();

push();
//rotation of purple ellipses (virus arm dots)
translate(213,210);
for (let j = 0; j < 10; j ++) {
  ellipse(20,90,30,15);
  fill(243,25,75);
  ellipse(20,90,30,15);
  ellipse(20,90,20,7);
  fill(189,5,43);
  ellipse(20,90,20,7);
  rotate(PI/5);
}
pop();

//making the sanitizer move with the mouse
imageMode(CENTER);
image(sanitizer,mouseX,mouseY);


  }
