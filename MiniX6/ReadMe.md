# MiniX6

<img src="data.png" width="600" height="400">

<img src="datas.png" width="600" height="400">

## URL to work in web browser (only works in chrome)
https://nikittameiniche.gitlab.io/aesthetic-programming/MiniX6/

## Link to my sketch file
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX6/sketch.js

## My reworked MiniX
I have decided to rework my MiniX4, as I feel that I could add something to it, other than what it already is. I chose this one, because I have more to say about the topic, and I feel that something needs to be reworked, in order to elaborate what I critisize in my program. 

In my program I changed some of the layout, while I also added this extra element of critique of the speech library that my program is built around. When you start talking, a red rectangle, the word "BEWARE" and different sentences pop up on the screen. These sentences are actually from Google´s terms of service (https://policies.google.com/terms?hl=en-US). The reason that I included these, is because when you use the speech library in P5, all received speech input is sent to google or the cloud, which is then run through their translator/text programs, and returned as text. I started thinking about the speech library, and found it sort of conflicting that the aim of my program was to criticize the datafication and the so called "hidden listener", while I used the speech library, which operates in exactly the same way. This is why the red elements are blinking, to tell you that, although this is a critical product, you as a user, agree to these terms, when you allow the microphone to record. With that said, I could not have made my program with the ressources already available in P5, and this is why I searched for an add on. For this sketch I thought about finding a program that had the same abilities, but without datafication. I came to the conclusion that this was an impossible task and I understood that we all use these programs, because they are easy, accesible and conveinient. Conveinience is probably the reason why no one has tried to make a non-datafied version of translate programs and such, although they are storing every piece of information that is put into them.  

## Aesthetic programming in my work
My work demonstrates an aesthetic programming thinking, as it gets the users mind going. It is not just code. My work is based on a deeper societal issue of datafication, which is highly relevant in our culture nowadays. In other words, this work is not aesthetic in the sense that it is visually pleasing or beautiful, but more so aesthetic in a political sense. Whether or notsomething is politically pleasing might be determinded by looking at its ability to please or live up to the political correctness of current time. I take this topic up to debate in my work, by questioning whether the extent of datafication is politically correct or if we should just accept it.    

## Programming as a practice
Programming has become more commom in society, as technology has developed, and the need for programmers has grown. Coding has become the new literacy, and it has changed the definition of what it means to be literate, as almost as important as being able to read and write regular words. Also, digital art is now accepted, and welcomed into the art foras, wheras previously people questioned this as an art form. Coding is a necissary skill to be relevant in this day and age. 

Programming is an important part of digtital culture. With programming we as programmers, can take part in shapinging the digital culture surrounding us. What is being programmed can end up leading to a sort of behavioural control over the people who use the things that have been programmed. All of these platforms that we use have been programmed to work in certain ways and to limit cetain actions. 

## Critical-aesthetics
A concept of critical making is to take a critical stance on a habit. This is something I try to do in my MiniX, where the habit is accepting and using the technologies available to us because of their conveinience. I try to take this critical stance, and highlight what is happening behind our screens and just how much is being captured and datafied. We as users, need to accept the terms of use lying behing these platforms, in order to use them and be apart of a community. We use them because of their conveinience and to maintain our social relations. On the other hand, we use pages like facebook out necessity, to communicate with others through messenger and to find important information regarding jobs and education in larger groups, connectiong people who have something in common. My work does not solve this problem of having to accept the terms of use, but it raises a debate about whether we should just stand back and pretend like the datafication is no problem at all.

In the **"Critical Making and interdisciplinary learning"** by Ratto and Hertz, I stumbled upon this quote: *"The concept of critical making has many predecessors, all of which start with the assumption that built technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture"* (p. 19). This notion of combining technological developement and cutural reflectivity in order to create something provocative, is exactly what we try to do in aesthetic programming, and I also try to make people re-think the role of technology, like they explain. In this MiniX what I want people to re-think when looking at my program, is the datafication that we are subjects to, when we use technology. I am not saying people should quit social meadia and the internet, but I would hope that my work would make them have a more critical look on what comes with using technology today.  

