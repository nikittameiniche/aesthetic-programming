//defining my variables
let iPhone;
let mic;
//variables for making text fade in+out (slower in the beginning)
let fade;
let fadeAmount = 4;

let myRec = new p5.SpeechRec(); // new P5.SpeechRec object
	myRec.continuous = true; // continuous voice recognition
	myRec.interimResults = true; // allows partial recognition

//loading photo of iPhone
function preload() {
  iPhone = loadImage('phone.png');
}

function setup(){
	createCanvas(880,640);
	angleMode(DEGREES);
	fade = 0

	//making the speech appear as text
	myRec.onResult = showSpeech //when something is being said, write it
	myRec.start(); // starts recording of voice

	//activating mic
	mic = new p5.AudioIn();
	mic.start();
}

function draw(){
	background(0);

	//inserting the iPhone
	push();
	translate(width/2,height/2);
	rotate(-18);
	imageMode(CENTER);
	image(iPhone,0,30);
	pop();

	//red ellipse grows according to the sound level of the input
	let vol = mic.getLevel();
	fill('red');
	noStroke();
	ellipse(width/2+15,height/2,vol*4000,vol*4000);

	//info text
	textSize(18);
	textAlign(CENTER);
	fill(255);
	textFont('courier');
	text("S T A R T",700,320);
	text("T A L K I N G",700,340);

  //running the showSpeech function
	showSpeech();
}

function showSpeech(){
  if(myRec.resultValue==true) {
		//rectangle grey
		fill(30);
		rectMode(CENTER);
		rect(680,height/2-40,220,440,20);

		//text appearing on the rectangle
		fill(255);
		textSize(12);
		textAlign(LEFT);
		textFont('courier');
		textStyle(NORMAL);
		//text(input,x,y,placement inside textbox of a certain size)
		text(myRec.resultString,680,height/2-40,160,380);

		//terms of use text (\n means jump to next line)
		fill(255,0,0,fade);
		textSize(12);
		textStyle(BOLD);
		text("We collect voice and audio information\nwhen you use our audio features",50,50);
		text("If you use our services to make and\nreceive calls we may collect\ninformation about time, date and\nduration of calls",50,190);
		text("We use your information to deliver our\nservices",50,380);
		text("We collect information about your\nlocation when you use our services\nfree of charge",50,540);
		text("We may show you personalized ads based\non your interests",50,120);
		text("We collect the content you create,\nupload, or receive when using our\nservices",50,290);
		text("When you share information publicly,\nyour content may become accessible\nthrough search engines",50,450);

    //BEWARE text
		textSize(70);
		textStyle(NORMAL);
		text("BEWARE",580,580);

		//red rectangle
		stroke(255,0,0,fade);
		strokeWeight(2);
		noFill();
		rectMode(CENTER);
		rect(width/2,height/2,840,600);

		//speed of fading (both text and rect)
		if (fade<0) fadeAmount = 8;
		if (fade>255) fadeAmount = -8;
		fade += fadeAmount;
		print(fade);
	}
}
