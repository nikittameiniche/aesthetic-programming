//REFERENCES
//bouncing ball video: https://youtu.be/LO3Awjn_gyU
//createGraphics() video: https://youtu.be/TaluaAD9MKA

//RULES
//if ball hits edge of rectangle, create cicle where it hit the reclangle
//on same occurence, create a circle on opposite side of the other size rectangle

let x = 0;
let y = 0;
//defining speeds of the two balls (x speed and y motion)
let speed = 4;
let motion = 2;
//defining the variable name for create graphics or "another canvas" on top
let cg;

function setup(){
  createCanvas(700,700);
  //creating the extra canvas on top
  cg = createGraphics(700,700);
  //making it clear so both canvases are visble - seeing through the top canvas
  cg.clear();
}

function draw(){
  background(0);
  strokeWeight(0.25);
  stroke(255);
  noFill();


//balls sideways
//for loop to make two with 400 distance
for (let i=50; i<500; i=i+400){
  let u = 100+i;
  ellipse(x+110,u,20,20);
}
//balls going up/down
//for loop to make two with 100 distance
for (let v=200; v<400; v=v+100){
  let t = 100+v;
  ellipse(t,y+210,20,20);
}

//rectangular frames settings
stroke(255);
strokeWeight(0.25);
noFill();
rectMode(CENTER);
//big rect
rect(width/2,height/2,500,500);
//small rect
rect(width/2,height/2,300,300);

//sideways balls speed
if (x>width-220){
  speed = -4;
  //cg. used to apply this to the top canvas only
  cg.noStroke();
  cg.fill(255,100);
  cg.circle(600,random(100,600),random(80));
  cg.fill(255,100);
  cg.circle(200,random(200,500),random(80));
}
if (x<0){
  speed = 4;
  cg.noStroke();
  cg.fill(255,100);
  cg.circle(100,random(100,600),random(80));
  cg.fill(255,100);
  cg.circle(500,random(200,500),random(80));
}

//up/down balls motion
if(y>height-420){
  motion = -2;
  cg.noStroke();
  cg.fill(255,100);
  cg.circle(random(200,500),500,random(80));
  cg.fill(255,100);
  cg.circle(random(100,600),100,random(80));
}
if (y<0){
  motion = 2;
  cg.noStroke();
  cg.fill(255,100);
  cg.circle(random(200,500),200,random(80));
  cg.fill(255,100);
  cg.circle(random(100,600),600,random(80));
}

//making the balls move with x + speed/motion
x = x + speed;
y = y + motion;
//makes the cg show on the screen
image(cg,0,0);
}
