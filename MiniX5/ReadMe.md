# MiniX5

<img src="generative.png" width="400" height="400">

**Video of the the generative program:**
![](generative.mov)

## URL to work in web browser
https://nikittameiniche.gitlab.io/aesthetic-programming/MiniX5/

## Link to my sketch file
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX5/sketch.js

## About this program
For this week´s MiniX I have created a generative program using for-loops and if-statements. I used the for-loops, to create multiple balls on the same axis and I used the if-statements to set the speed of the balls. I learnt how to make the balls move from seeing this video (https://youtu.be/LO3Awjn_gyUEach). Each if-statement runs if x or y is a certain value, making the ball change direction each time it hits the edge of the rectangles. Alongside this action, each time the if-statement is true, the program generates random ellipses on the rectangles. Making this happen was a bit of a challenge for me, as I realised that I couldnt have both the moving ball and the static circles appear at the same time. Because of this I did some research and found a video, that made me realize, that I needed to use the createGraphics() function, in order to make different things happen on two seperate canvases (https://youtu.be/TaluaAD9MKA).  

## Rules of my generative program
The rules of my program are as follows:
1. If the balls hit the side of the rectangle, then print a random sized ellipse in a random spot on this side. 
2. Also, on same occurence, print a random sized ellipse in a random spot on the opposite side of the other rectangle.
3. The balls should always continue to move and will create keep on generating new ellipses.

With these rules my generative program creates two frame-like structures, consisting of random sized white ellipses. At first the program will look more random, but as it progresses it will generate a more recognizable structure. The program produces emergent behaviour, as I have created an environment with a set of rules, that will always apply and generate something new on the canvas. You might however be able to predict how the program will look over time, even though the placement and sizes of the ellipses are randomized. 

The rules are important for my generative program as they are what makes it generate the things it does and where it does. The process of this program goes on for a while, before it becaomes clear what is being generated and how it will look, unlike some of the other generative works that we have looked at for this week. If we use the 10-print program (https://10print.org) as an example, this artpiece does come to an end quite fast, and nothing new happens on the canvas after a while. My program keeps on generating something new and adding to the canvas for as long as you let it run. Therefore the process is important for my work as the artwork changes over time, creating a slightly different visual.

## The idea of an auto-generator
This MiniX has helped me understand what is meant by an auto-genrator. I find it interesting how you can create all of this abstract art using your computer, by giving it some rules that it should run by. During the process I began to wonder if abstract art is even abstract? I mean how is it abstract, when we ourselves choose the lenght of our strokes or the colors that we dip our paintbrush into? In some way i feel that computer generated art is more abstract, as it allows for random selection of different factors. Maybe we are just calling things abstract, when they do not look like anything specific. The auto-generative art has risen in popularity, and I get why, because it is impressive what the computer can generate, and it is different because in someway we are not the ones who make the choices on how the art will end up looking. I stumbled upon a quote in the book "Aesthetic Programming" by Soon and Cox, 2020, that said: "Generative art refers to any practice where artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art." (Chapter 5 - Auto-generator, p. 126). Here, the human is reffered to as the artist, even when the computer is creating the art. I began to reflect about this, as this means that we are still artists using another medium to create our art. We are still the mind behind the works, but can we take all the creadit? Are all of the decisions ours? Or is most of the work being done by the computer?


