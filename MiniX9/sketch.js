let api = "https://api.giphy.com/v1/stickers/search";
let apiKey = "&api_key=5pJi4ZAdU88Z0st9QBQWv2nYWeFWgvpE";
let query = "?q=food";
let giphy;
let gif;
let gif2;
let gif3;
let font;


let url = "https://api.giphy.com/v1/stickers/search?q=food&api_key=5pJi4ZAdU88Z0st9QBQWv2nYWeFWgvpE";

function preload(){
 giphy = loadJSON(url);
 font = loadFont('Winkle.ttf');
}

function setup(){
createCanvas(windowWidth,windowHeight);
background(250,200,200);
loadJSON(url,gotData);
}

function gotData(giphy){

  gif = createImg(giphy.data[int(random(0,16))].images.fixed_width.url);
  gif.style('position','absolute');
  gif.style('top','300px');
  gif.style('left','200px');

  gif2 = createImg(giphy.data[int(random(17,34))].images.fixed_width.url);
  gif2.style('position','absolute');
  gif2.style('top','300px');
  gif2.style('left','500px');

  gif3 = createImg(giphy.data[int(random(35,49))].images.fixed_width.url);
  gif3.style('position','absolute');
  gif3.style('top','300px');
  gif3.style('left','800px');
}

function draw(){
fill(211,254,255);
stroke(255);
strokeWeight(8);
quad(50,50,width-50,50,width-50,height-50,50,height-50);
fill(255,100,100);
stroke(255);
textSize(80);
textAlign(CENTER);
textFont(font);
text("MENU OF THE DAY",width/2,200);
imageMode(CENTER);
image(gif,width/2,height/2);
image(gif2,width/2,height/2);
image(gif3,width/2,height/2);
}
