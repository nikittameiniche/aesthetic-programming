//defining my variables
let iPhone;
let mic;
let myRec = new p5.SpeechRec(); // new P5.SpeechRec object
	myRec.continuous = true; // continuous voice recognition
	myRec.interimResults = true; // allows partial recognition

//loading photo of iPhone
function preload() {
    iPhone = loadImage('phone.png');
}

function setup(){
	createCanvas(880,640);
	background(0);
	angleMode(DEGREES);

	//text in the top
	textSize(30);
	textAlign(CENTER);
    fill('white');
	textFont('courier');
	text("S T A R T  S P E A K I N G", width/2+10, 70);

	//making the speech appear as text
	myRec.onResult = showSpeech //when something is being said, write it
	myRec.start(); // starts recording of voice

	//activating mic
	mic = new p5.AudioIn();
	mic.start();
}

function draw(){

	//inserting the iPhone
	push();
	translate(width/2,height/2);
	rotate(-18);
	imageMode(CENTER);
	image(iPhone,0,30);
	pop();

	//red ellipse grows according to the sound level of the input
	let vol = mic.getLevel();
	fill('red');
	noStroke();
	ellipse(width/5,height/2,vol*3000,vol*3000);
}


function showSpeech(){
if(myRec.resultValue==true) {
	
	background(0);

	//rectangle grey
	fill(30);
	rectMode(CENTER);
	rect(width/4*3,height/2-40,220,440,20);

	//text appearing on the rectangle
	fill(255);
	textSize(12);
	textAlign(LEFT);
	textFont('courier');
	//text("what",x,y,placement inside textbox of a certain size)
	text(myRec.resultString, width/4*3,height/2-40,160,380);
 }
}
