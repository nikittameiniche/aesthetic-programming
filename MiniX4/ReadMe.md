# MiniX4

<img src="program2.png" width="590" height="400">

<img src="program1.png" width="590" height="400">

## URL to work in web browser
https://nikittameiniche.gitlab.io/aesthetic-programming/MiniX4/

## Link to sketch file
https://gitlab.com/nikittameiniche/aesthetic-programming/-/blob/master/MiniX4/sketch.js

## Submission to transmediale 2015

**Title:** *"The hidden listener"*

**Description:** The purpose of this work is to shed light on the fact that we are all victims of the data capturing that is happening in the world. When we thing about data capturing, most of us are not to worried, because we think of the data that we provide others with, when we go shopping online or like and look at certain things on social media. There is so much more to it than that. That is why I, with this work, try to show how our conversations are being recorded, although we might think that a turned off phone equals privacy. "The hidden listener" is always there to pick up on your conversations, and no matter the quality or acuracy of what is being heard, companies like google will certainly be able to use it to their advantage. This work will make you realise how much of what you say is being heard, and will make you reflect about whether you are okay with being overheard or if it scares you how much they probably know about you.

## My program and its relation to Data Capturing

**How I made my program:** 
To create this program I have used the library p5.speech downloaded from this site (https://github.com/IDMNYU/p5.js-speech), to create the element of voice recognition. On this page I found example code showing how the library could be used, but none of these seemed to work for me, which was frustrating. So I ended up mixing some of the lines of code from various examples on this site and from a Daniel Shiffmann video (https://www.youtube.com/watch?v=q_bXBcmfTJM), which ended up working after many hours of coding. I used the speech recognition to allow for the user to make an input using their own voice, and make it seem like your voice is being recorded by the iPhone while it is turned off. The output (the words being heard) appears on a sort of hologram of the phone, that is supposed to symbolise what happens behind the black screen. I also used the p5.sound, to map the volume of the mic input to the red ellipse, to create an association to the well known red blinking dot, that usually appears when you record something. I have learnt a lot about how to work with sound and speech, which I feel are very useful things in programming. 

**How my program adresses the theme of capture all:** 
My work does not capture everything in terms of how many types of data are being captured. It captures all that is being said by the owner of the phone, sharing all your conversations with companies like Google, who can then make money off of your personal data. What I wanted to communicate with this work was how with technology being here, and being part of all of our lives, nothing seems to be private anymore. My aim with this program is to make people feel uncorfortable, when they realise that the recording of our conversations is actually happening all the time. I don´t think many people know to what extent our data is being captured, but with this work, I hope more people will start to question the life we are living, and if everything is as harmless as we might think. 

**Cultural implications of data capture:**
I think the biggest companies care only about one thing, and that is money. How can they use us to their benefit? We have been blessed with all of these technologial devices throughout the years, and we use them frequently, many might even say that they make our lives easier. It would however be relevant to address the theme of surveillance capitalism, which is elaborated by Shoshana Zuboff in this documentary (https://youtu.be/hIXhnWUmMvw). In this video, she talks about how the datafication process has evolved over time, and about surveillance capitalism as an economic model with the purpose of making money off of all of our pesonal data. The act of monitoring people´s behavior and conversations is more common in certain cultures and less accepted in others, but the thing is that none of us have done anything to stop what is going on. She goes in to detail about how we sort of know what is going on, but not the whole truth, which I think, might be one of the reasons that most of us are´nt too worried and are not actively protesting against our data being harvested by big companies to their advantage. It is wrong that we are being lied to, and the act of datafication threatens the basic rights of the self, because we have no privacy and we are being manipulated into making different choices based on what is desired by the ones monitoring us. 

